﻿using System;
using System.Xml;
using ClosedXML.Excel;
using System.Collections;

namespace XMLEditCoordinatePipes
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathXlsx = @"d:\Projects\C#\XML\ConsoleAppXML\pipeID.xlsx";
            string pathXML = @"d:\Projects\C#\XML\ConsoleAppXML\HydroModelforStend1.xml";
            ArrayList arrayList;
            //подключимся ко 2-му листу Эксельки и считаем данные pipeID
            XLWorkbook xLWorkbook = new XLWorkbook(pathXlsx);
            IXLWorksheet worksheet = xLWorkbook.Worksheet(2);
            arrayList = AddIDElementArray(worksheet, 1);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathXML);
            XmlNodeList xmlNodeList = xmlDoc.GetElementsByTagName("pipe");
            SetPipeCoordinateHeadNPS(xmlNodeList, arrayList, worksheet);

            //получим ссылку на 3-й лист в Excel
            worksheet = xLWorkbook.Worksheet(3);
            xmlNodeList = xmlDoc.GetElementsByTagName("pipe");
            arrayList = AddIDElementArray(worksheet, 1);
            SetPipeCoordinateHeadNPS(xmlNodeList, arrayList, worksheet);

            string[] elements = new string[8]
            {
                "blockValve", "tee", "break", "sensor", "sensor", "sensor", "sensor", "sensor"
            };

            int[] column = new int[8]{3, 3, 3, 2, 3, 2, 3, 3};
            int[] ident = new int[8] {1, 1, 1, 2, 2, 2, 2, 2};

            for (int i = 0; i < elements.Length; i++)
            {
                xmlNodeList = xmlDoc.GetElementsByTagName(elements[i]);
                arrayList = AddIDElementArray(worksheet, i + 4);
                SetCoordinate(xmlNodeList, arrayList, worksheet, column[i], ident[i]);
            }

            xmlDoc.Save(pathXML);
            Console.WriteLine("Операция завершена. Об успешности судить Вам");
        }

        static ArrayList AddIDElementArray(IXLWorksheet worksheet, int column)
        {
            int i = 2;
            ArrayList arrayList = new ArrayList();
            while (worksheet.Cell(i, column).Value.ToString() != "end")
            {
                arrayList.Add(worksheet.Cell(i, column).Value.ToString());
                i++;
            }
            return arrayList;
        }

        static void SetCoordinate(XmlNodeList xmlNodeList, ArrayList arrayList, IXLWorksheet worksheet, int columnCoordinate, int ident)
        {
            //Получаем все атрибуты необходимого элемента 
            XmlAttributeCollection atrCol;
            bool fl = false;
            string coordinate = String.Empty;
            foreach (XmlNode node in xmlNodeList)
            {
                atrCol = node.Attributes;
                fl = false;
                foreach (XmlAttribute attribute in atrCol)
                {
                    for (int i = 0; i < arrayList.Count; i++)
                    {
                        if (attribute.Value == arrayList[i].ToString())
                        {
                            fl = true;
                            coordinate = worksheet.Cell(i + 2, columnCoordinate).Value.ToString();
                            break;
                        }
                    }
                    if (!fl)
                        break;
                    switch (ident)
                    {
                        case 1:
                            if (attribute.Name == "coordinate")
                                attribute.Value = coordinate;
                            break;
                        case 2:
                            if (attribute.Name == "sensorCoordinate")
                                attribute.Value = coordinate;
                            break;
                    }
                    
                }
            }
        }

        static void SetPipeCoordinateHeadNPS(XmlNodeList xmlNodeList, ArrayList arrayList, IXLWorksheet worksheet)
        {
            //Получаем все атрибуты элемента pipe
            XmlAttributeCollection atrCol;
            bool fl = false;
            string coordinateStart = null, coordinateEnd = null;
            foreach (XmlNode node in xmlNodeList)
            {
                atrCol = node.Attributes;
                fl = false;
                foreach (XmlAttribute attribute in atrCol)
                {
                    for (int i = 0; i < arrayList.Count; i++)
                    {
                        if (attribute.Value == arrayList[i].ToString())
                        {
                            fl = true;
                            coordinateStart = worksheet.Cell(i + 2, 2).Value.ToString();
                            coordinateEnd = worksheet.Cell(i + 2, 3).Value.ToString();
                            break;
                        }
                    }
                    if (!fl)
                        break;
                    if (attribute.Name == "coordinateStart")
                        attribute.Value = coordinateStart;
                    if (attribute.Name == "coordinateEnd")
                        attribute.Value = coordinateEnd;
                }
            }
        }


    }
}

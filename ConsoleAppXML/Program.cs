﻿using System;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using ClosedXML;
using ClosedXML.Excel;
using System.Threading;


namespace ConsoleAppXML
{
    class Program
    {
        static void Main(string[] args)
        {
            //string pathTXT = @"d:\Projects\C#\XML\ConsoleAppXML\pipeID.txt";
            string pathXlsx = @"d:\Projects\C#\XML\ConsoleAppXML\pipeID.xlsx";
            string pathXML = @"d:\Projects\C#\XML\ConsoleAppXML\HydroModelforStend12345678.xml";
            string regpattern = @"pipe id=\W\w+\W";
            string regpatternchild = @"\d+";
            string regpatternCoordinateStart = @"coordinateStart=\W\d+\W";
            string regpatternCoordinateEnd = @"coordinateEnd=\W\d+\W";
            string pipe;
            int j;
            StringBuilder pipesID;

            FileInfo fileInfo = new FileInfo(pathXlsx);
            if (!fileInfo.Exists)
            {
                //FileStream fileStream = fileInfo.Create();
                //fileStream.Close();
                //Thread.Sleep(2000);
                Console.WriteLine("Отсутсвует необходимый файл");
                goto end;
            }
            else
            {
                Console.WriteLine("файл уже создан");
            }

            //using (StreamWriter writeTxt = new StreamWriter(pathTXT))
            //{
            //    using(StreamReader writeXml = new StreamReader(pathXML))
            //    {
            //        Match match;

            //        Regex regex = new Regex(regpattern);
            //        while (!writeXml.EndOfStream)
            //        {
            //            pipe = writeXml.ReadLine();
            //            if (regex.IsMatch(pipe))
            //            {
            //                match = regex.Match(pipe);
            //                writeTxt.WriteLine(match.Value, true);
            //            }
            //        }
            //    }
            //}

            using (XLWorkbook xLWorkbook = new XLWorkbook(pathXlsx))
            {
                IXLWorksheet worksheet = xLWorkbook.Worksheet(1);
                using (StreamReader writeXml = new StreamReader(pathXML))
                {
                    Match match;
                    Regex regex = new Regex(regpattern);
                    Regex regChild = new Regex(regpatternchild);
                    Regex regCoorStart = new Regex(regpatternCoordinateStart);
                    Regex regCoorEnd = new Regex(regpatternCoordinateEnd);
                    worksheet.Cell(1, 1).Value = "pipeID";
                    worksheet.Cell(1, 2).Value = "coordinateStart";
                    worksheet.Cell(1, 3).Value = "coordinateEnd";
                    worksheet.Cell(1, 4).Value = "diffrence";
                    j = 2;
                    while (!writeXml.EndOfStream)
                    {
                        pipe = writeXml.ReadLine();
                        RegMethod(regex, regChild, pipe, worksheet, ref j, 1);
                        RegMethod(regCoorStart, regChild, pipe, worksheet, ref j, 2);
                        RegMethod(regCoorEnd, regChild, pipe, worksheet, ref j, 3);
                    }
                    xLWorkbook.Save();
                }
            }
end:        Console.WriteLine("Операция завершена");
        }

        static void RegMethod(Regex regex, Regex regChild, string pipe, IXLWorksheet worksheet, ref int j, int column)
        {
            Match match;
            if (regex.IsMatch(pipe))
            {
                match = regex.Match(pipe);
                if (regChild.IsMatch(match.Value))
                {
                    match = regChild.Match(match.Value);
                    worksheet.Cell(j, column).Value = match.Value;
                    if (column == 3)
                    {
                        j++;
                        worksheet.Cell(j - 1, 4).Value = (Int32.Parse(worksheet.Cell(j - 1, 3).Value.ToString()) - Int32.Parse(worksheet.Cell(j - 1, 2).Value.ToString())) as Object;
                    }
                }
            }
        }
    }
}
